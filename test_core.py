from subprocess import getoutput as run
import os
import json

CURRENT_PATH = os.path.abspath('.')
API_URL = "http://localhost:8000/api/movies"


def test():
    command = './core.py'
    assert run(command) == 'Invalid URL.'

    command += ' --url ' + API_URL
    assert run(command) == """Request succeed.\nStatus code: 200\nResponse content was saved at {}/data.json""".format(CURRENT_PATH)

    assert run(command + ' --method GET') == """Request succeed.\nStatus code: 200\nResponse content was saved at {}/data.json""".format(CURRENT_PATH)

    headers = ' --headers \'{"accept": "text/html"}\''
    assert run(command + ' --method GET' + headers) == """Request succeed.\nStatus code: 200\nResponse content was saved at {}/data.html""".format(CURRENT_PATH)

    body = json.dumps({
        "imdbID": "test",
        "Title": "test",
        "Plot": "test",
        "Poster": "test"
    })

    assert run(command + ' --method POST' + ' --body \'{}\''.format(body)) == """Request succeed.\nStatus code: 201\nResponse content was saved at {}/data.json""".format(CURRENT_PATH)
    assert run(command + ' --method POST' + ' --body \'{}\''.format(body)) == """Request failed with client errors.\nStatus code: 400\nResponse content was saved at {}/data.json""".format(CURRENT_PATH)

    body = json.dumps({
        "imdbID": "test",
        "Title": "test",
        "Plot": "test",
        "Poster": "new"
    })
    assert run(command + '/test' + ' --method PUT' + ' --body \'{}\''.format(body)) == """Request succeed.\nStatus code: 200\nResponse content was saved at {}/data.json""".format(CURRENT_PATH)

    body = json.dumps({
        "Poster": "patch update"
    })
    assert run(command + '/test' + ' --method PATCH' + ' --body \'{}\''.format(body)) == """Request succeed.\nStatus code: 200\nResponse content was saved at {}/data.json""".format(CURRENT_PATH)

    assert run(command + '/test' + ' --method DELETE') == """Request succeed.\nStatus code: 204\nResponse has no content."""

    assert run(command + ' --method HEAD') == """Request succeed.\nStatus code: 200\nResponse has no content."""

    assert run(command + ' --method OPTIONS') == """Request succeed.\nStatus code: 200\nResponse content was saved at {}/data.json""".format(CURRENT_PATH)

    assert run(command + 'invalid_url') == """Request failed with client errors.\nStatus code: 404\nResponse content was saved at /home/tthanh/workspace/restful-api-introduction/data.html"""

    assert '200' in run("./core.py --url 'http://www.omdbapi.com/?apikey=81d3b711&s=Avengers'")

    print("All tests passed.")


if __name__ == '__main__':
    test()
