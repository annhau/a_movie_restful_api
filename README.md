# A RESTful Movie Project
This project was made when I started learning **Django** and **django-rest-framework**. It was inspired by [OMDb API](https://www.omdbapi.com/) - a RESTful web service to obtain movie information


The **omdb_project** provides web UI for everybody to search for a movie and view its information. It also has the API (using **Django** only) for those.
And in the **rest_movie_project**, I rewrite the API with **rest_framework**.


## Installation
Ubuntu: source venv/bin/activate

Windows: pip install -r requirements.txt


## Testing
**core.py**: To make request to the API (similar to cURL).

**test_core.py**: test all the flow in a function.

**unit_test_core.py**: use unittest to test a single API at a time.

## API
> python rest_movie_project/manage.py runserver
### Demo
![img.png](img.png)

## Web application
> python omdb_project/manage.py runserver 7000

### Demo
![img_1.png](img_1.png)

![img_2.png](img_2.png)
