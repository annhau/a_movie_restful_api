from rest_framework.generics import GenericAPIView
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin, \
    CreateModelMixin, DestroyModelMixin, UpdateModelMixin
from api.filters import TitleFilterBackend
from .models import Movie
from .serializers import MovieSerializer


class MovieCollectionView(ListModelMixin, GenericAPIView, CreateModelMixin):
    """
    get: Select all the movies in the database

    post: Create new movie
    """
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    filter_backends = TitleFilterBackend,

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class MovieEntityView(UpdateModelMixin, RetrieveModelMixin, DestroyModelMixin,
                      GenericAPIView):
    """
    get:
    Get a movie information

    post:
    Full update a movie in the database

    patch:
    Partial update a move in the database

    delete:
    Delete a movie in the database

    """
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
