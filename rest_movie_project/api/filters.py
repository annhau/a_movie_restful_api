import coreapi
from rest_framework.filters import BaseFilterBackend


class TitleFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        """
        Filter queryset with insensitive contain strategy
        :param request:
        :param queryset:
        :param view:
        :return:
        """
        search_query = request.query_params.get('title', None)
        if search_query:
            queryset = queryset.filter(Title__icontains=search_query)
        return queryset

    def get_schema_fields(self, view):
        """
        Add field to schema
        :param view:
        :return:
        """
        return [coreapi.Field(
            name='title',
            location='query',
            required=False,
            type='string',
            description='Filter result by movie\'s title',
            example='Harry Potter',
        )]
