from django.urls import path

from api import views


urlpatterns = [
    path('api/movies', views.MovieCollectionView.as_view(), name='movies-collection'),
    path('api/movies/<pk>', views.MovieEntityView.as_view(), name='movies-entity'),
]
