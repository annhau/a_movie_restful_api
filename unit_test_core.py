import unittest
from subprocess import getoutput as run
import json


def assertAllIn(words, string):
    if not all([word in string for word in words]):
        raise AssertionError(', '.join(words) + ' not in:\n"' + string + '"')


class TestCore(unittest.TestCase):
    command = './core.py --url http://localhost:8000/api/movies{} --method {}'

    def create_test_post(self):
        body = json.dumps({
            "imdbID": "test",
            "Title": "test",
            "Plot": "test",
            "Poster": "test"
        })
        command = self.command.format('', 'POST') + ' --body \'{}\''.format(body)
        return run(command)

    def delete_test_post(self):
        command = self.command.format('/test', 'DELETE')
        return run(command)

    def test_no_params(self):
        self.assertEqual('Invalid URL.', run('./core.py'))

    def test_get(self):
        expected = ['succeed', '200', 'saved']
        assertAllIn(expected, run(self.command.format('', 'GET')))

    def test_fail_get(self):
        expected = ['failed', 'client', '404', 'saved']
        assertAllIn(expected, run(self.command.format('/wrong', 'GET')))

    def test_post(self):
        self.delete_test_post()
        expected = ['succeed', '201', 'saved']
        assertAllIn(expected, self.create_test_post())

    def test_fail_post(self):
        self.create_test_post()
        expected = ['failed', 'client', '400', 'saved']
        assertAllIn(expected, self.create_test_post())

    def test_put(self):
        body = json.dumps({
            "imdbID": "test",
            "Title": "new title",
            "Plot": "test",
            "Poster": "test"
        })
        expected = ['succeed', '200', 'saved']
        command = self.command.format('/test', 'PUT')
        command += ' --body \'{}\''.format(body)
        assertAllIn(expected, run(command))

    def test_fail_put(self):
        self.create_test_post()
        body = json.dumps({
            "imdbID": "test",
            "Plot": "new plot",
            "Poster": "test"
        })
        expected = ['failed', 'client', '400', 'saved']
        command = self.command.format('/test', 'PUT')
        command += ' --body \'{}\''.format(body)
        assertAllIn(expected, run(command))

    def test_patch(self):
        self.create_test_post()
        body = json.dumps({
            "Title": "another title",
        })
        expected = ['succeed', '200', 'saved']
        command = self.command.format('/test', 'PATCH')
        command += ' --body \'{}\''.format(body)
        assertAllIn(expected, run(command))

    def test_delete(self):
        self.create_test_post()
        expected = ['succeed', '204', 'no content']
        assertAllIn(expected, self.delete_test_post())

    def test_options(self):
        expected = ['succeed', '200', 'saved']
        command = self.command.format('', 'OPTIONS')
        assertAllIn(expected, run(command))

    def test_head(self):
        expected = ['succeed', '200', 'no content']
        command = self.command.format('', 'HEAD')
        assertAllIn(expected, run(command))


if __name__ == '__main__':
    unittest.main()
