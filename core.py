#!/usr/bin/env python3
import requests
import json
import os
import argparse

METHOD_MAP = {'GET': requests.get, 'POST': requests.post,
              'PUT': requests.put, 'DELETE': requests.delete,
              'PATCH': requests.patch, 'HEAD': requests.head,
              'OPTIONS': requests.options}


def get_arguments():
    """
    Get arguments
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--method', default='GET', help='HTTP Method')
    parser.add_argument('--url', default='', help='API url and parameters')
    parser.add_argument('--headers',
                        help='Request headers (require json format)')
    parser.add_argument('--body', help='Request body (require json format)')

    return parser.parse_args()


def handle_arguments():
    """
    Validate arguments
    """
    args = get_arguments()
    url, method, headers, body = args.url, args.method, args.headers, args.body
    if args.method not in METHOD_MAP:
        exit('Invalid method.')
    try:
        if headers:
            headers = json.loads(headers)
        else:
            headers = {}
    except json.decoder.JSONDecodeError:
        print(headers)
        exit('Invalid headers.')
    try:
        if body:
            body = json.loads(body)
        else:
            body = {}
    except json.decoder.JSONDecodeError:
        exit('Invalid body.')

    return url, headers, method, body


def save_response_content(response):
    """
    Get response content's type and save it
    """
    if not response.text:
        return None

    content_type = response.headers.get('Content-Type')
    content_type = content_type.split(';')[0].split('/')[-1].strip()
    if content_type != 'plain':
        file_name = 'data.' + content_type
    else:
        file_name = 'data.txt'

    with open(file_name, 'w') as f:
        if content_type == 'json':
            out = json.dumps(response.json(), indent=4)
        else:
            out = response.text
        f.write(out)
    return file_name


def request_to_api(url, headers, method, body):
    """
    Make a request to the url and save the response
    """
    try:
        response = METHOD_MAP.get(method)(url, headers=headers, json=body)
        status_code = response.status_code

        if status_code in range(200, 300):
            print('Request succeed.')
        elif status_code in range(400, 500):
            print('Request failed with client errors.')
        elif status_code in range(500, 600):
            print('Request failed with server errors.')
        else:
            print('Request was redirected.')
        print('Status code:', status_code)

        file_path = save_response_content(response)
        if file_path:
            current_path = os.path.abspath('.')
            file_path = os.path.join(current_path, file_path)
            print('Response content was saved at ' + file_path)
        else:
            print('Response has no content.')
    except Exception:
        exit('Invalid URL.')


def main():
    request_to_api(*handle_arguments())


if __name__ == '__main__':
    main()
