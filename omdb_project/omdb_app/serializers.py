from rest_framework import serializers
from .models import *


class SearchResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = 'Title', 'Year', 'imdbID', 'Poster'


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = '__all__'
