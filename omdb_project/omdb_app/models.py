from django.db import models


class Movie(models.Model):
    imdbID = models.CharField(max_length=255, primary_key=True)
    Title = models.CharField(max_length=255)
    Year = models.CharField(max_length=255)
    Genre = models.CharField(max_length=255)
    imdbRating = models.CharField(max_length=255)
    Actors = models.CharField(max_length=255)
    Director = models.CharField(max_length=255)
    Plot = models.CharField(max_length=255)
    Type = models.CharField(max_length=255)
    Poster = models.CharField(max_length=255)

    def __str__(self):
        return self.Title
