from django.apps import AppConfig


class OmdbAppConfig(AppConfig):
    name = 'omdb_app'
