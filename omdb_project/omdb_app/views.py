from django.http import JsonResponse, Http404
from django.shortcuts import render
from rest_framework.decorators import api_view

from . import movie_api
from .models import *
from .serializers import SearchResultSerializer, MovieSerializer


def index(request):
    movies = []
    if request.method == 'POST':
        movie_name = request.POST.get('name', '')
        movies = movie_api.search_rest_movies(movie_name)

    return render(request, 'index.html', context={'movies': movies})


def detail(request, imdb_id):
    movie = movie_api.get_rest_movie_detail(imdb_id)
    if movie:
        return render(request, 'detail.html', context={'movie': movie})
    raise Http404


@api_view(['GET'])
def api(request):
    """
    Simulate the OMDB API
    :param request:
    :return:
    """
    GET = request.GET

    # Validate api key
    api_key = GET.get('apikey', '')
    if api_key != '1024':
        return JsonResponse({"Response": "False", "Error": "Invalid API key!"})

    if 's' in GET:
        # s case
        search_query = GET.get('s', '')
        if search_query:
            result = Movie.objects.filter(Title__icontains=search_query)
            if result:
                serializer = SearchResultSerializer(result, many=True)
                return JsonResponse({'Search': serializer.data})
    elif 't' in GET:
        # t case
        search_query = GET.get('t', '')
        if search_query:
            result = Movie.objects.filter(Title__icontains=search_query).first()
            if result:
                serializer = MovieSerializer(result)
                return JsonResponse(serializer.data)
    elif 'i' in GET:
        # i case
        search_query = GET.get('i', '')
        if search_query:
            result = Movie.objects.get(imdbID=search_query)
            if result:
                serializer = MovieSerializer(result)
                return JsonResponse(serializer.data)
    else:
        # required parameters is not provided
        return JsonResponse(
            {"Response": "False", "Error": "Something went wrong."})

    # Movie not found cases
    return JsonResponse({"Response": "False", "Error": "Movie not found!"})
