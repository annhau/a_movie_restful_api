"""
Make request to the API and get response content
Using the simulation version
"""

import requests

OMDB_URL = 'http://www.omdbapi.com/'
ROOT_URL = 'http://localhost:7000/api'
ROOT_REST_URL = 'http://localhost:8000/api/movies'


def search_movies(keyword):
    """
    Search for movies
    :param keyword:
    :return:
    """
    url = ROOT_URL + '?apikey=81d3b711&s=' + keyword
    response = requests.get(url)
    if response.status_code == 200:
        if 'Search' in response.json():
            return response.json()['Search']
    return []


def get_movie_detail(imdb_id):
    """
    Get movie detail by id
    :param imdb_id:
    :return:
    """
    url = ROOT_URL + '?apikey=81d3b711&i=' + imdb_id
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    return {}


def search_rest_movies(keyword):
    """
    Search for movies
    :param keyword:
    :return:
    """
    url = ROOT_REST_URL + '?title=' + keyword
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()["results"]
    return []


def get_rest_movie_detail(imdb_id):
    """
    Get movie detail by id
    :param imdb_id:
    :return:
    """
    url = ROOT_REST_URL + '/' + imdb_id
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    return {}
